# Use an official WordPress image as the base image
FROM wordpress:latest

# Copy the entire WordPress project into the container
COPY . /var/www/html

# Set the working directory
WORKDIR /var/www/html

# Expose port 80 (default for HTTP)
EXPOSE 80